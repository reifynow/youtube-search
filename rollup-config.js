import rollup      from 'rollup'
import nodeResolve from 'rollup-plugin-node-resolve'
import commonjs    from 'rollup-plugin-commonjs';
import uglify      from 'rollup-plugin-uglify'
export default {
  entry: 'src/main-aot.js',
  dest: 'dist/build.js', // output a single application bundle
  sourceMap: false,
  format: 'iife',
  onwarn: function (warning) {
    // Skip certain warnings
    // should intercept ... but doesn't in some rollup versions
    if (warning.code === 'THIS_IS_UNDEFINED') {
      return;
    }
    // console.warn everything else
    console.warn(warning.message);
  },
  plugins: [
    nodeResolve({jsnext: true, module: true}),
    commonjs({
      include: [
        'node_modules/rxjs/**',
        'node_modules/text-mask-core/dist/textMaskCore.js',
        'node_modules/text-mask-addons/dist/createNumberMask.js',
        'node_modules/angular2-text-mask/dist/angular2TextMask.js',
        'node_modules/primeng/**'
      ],
      namedExports: {
        'node_modules/primeng/primeng.js': ['AutoCompleteModule', 'CalendarModule', 'ListboxModule', 'DropdownModule', 'CheckboxModule', 'PanelModule', 'ButtonModule', 'InputTextModule']
      }
    }),
    uglify()
  ]
}
