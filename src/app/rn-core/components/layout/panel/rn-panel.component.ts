import {Component, Input} from '@angular/core';

export {RnPanelBody} from './rn-panel-body.component';
export {RnPanelHeader} from './rn-panel-header.component';

@Component({
  selector: 'rn-panel',
  template: `
    <div>
      <div class="rn-panel" [style]="style"><ng-content></ng-content></div>
    </div>
  `,
  styleUrls: ['./rn-panel.css']
})
export class RnPanel {
  @Input() style : string = '';
  constructor() {}
}
