import {Injectable} from '@angular/core';
import {Router, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';
import {Observable, ReplaySubject} from 'rxjs/Rx';

export class BcRouteItem {
  route: any = ['/'];
  name: string = '';
  snapshot: ActivatedRouteSnapshot;

  constructor(name: string, snapshot: ActivatedRouteSnapshot, route: any = null) {
    this.snapshot = snapshot;
    this.name = name;
    if (!route) this.route = this.linkChildren(snapshot);
    else this.route = route;
  }

  updateSnapshot(snapshot : ActivatedRouteSnapshot) {
    this.snapshot = snapshot;
    this.route = this.linkChildren(snapshot);
  }

  updateName(name : string) {
    this.name = name;
  }

  private linkChildren(children: any, routeArray: Array<string> = []): Array<string> {
    /*    if (!children || !children.length) return routeArray;
     if (children[0].url.length) {
     routeArray = routeArray.concat(children[0].url.map(url => url.path));
     if (Object.keys(children[0].url[0].parameters).length) {
     routeArray.push(children[0].url[0].parameters);
     }
     }
     return this.linkChildren(children[0].children, routeArray);
     }*/
    if (children.pathFromRoot.length) {
      children.pathFromRoot.forEach(item => {
        if (item.url.length) {
          routeArray = routeArray.concat(item.url.reduce((group : Array<any>, el : any) => {
            group.push(el.path);
            let params = {};
            for (let key in el.parameters) {
              params[key] = el.parameters[key];
            };
            if (Object.keys(params).length)
              group.push(params);

            return group;
          }, []));
        }
      });
    }
    if (routeArray.length) routeArray[0] = '/'+routeArray[0];
    return routeArray;
  }
}

@Injectable()
export class BreadcrumbService {
  routes : Array<BcRouteItem> = [];
//  routeMap : any = {};
  getRoutes : ReplaySubject<any> = new ReplaySubject(1);

  constructor(private router : Router) {

  }

  addRouteItem(item : BcRouteItem) : void {
//    if (this.routes.some((itm : BcRouteItem) => itm.snapshot === item.snapshot)) return;

    this.routes.push(item);
//    this.routeMap[this.getRouteKey(item)] = item;

    this.getRoutes.next(this.routes);
  }

  removeRouteItem(item : BcRouteItem) : void {
    let idx = this.routes.indexOf(item);
    if (~idx) this.routes.splice(idx, 1);
//    if (this.routeMap.hasOwnProperty(this.getRouteKey(item))) delete this.routeMap[this.getRouteKey(item)];

    this.getRoutes.next(this.routes);
  }

/*  private getRouteKey(item: BcRouteItem) : string {
    return item.route+':'+item.name;
  } */
}
