import {NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {ImageCropperModule} from './image-cropper/index';
import {ImageCropperModal} from './input/image-cropper.component';
import {RnPanel, RnPanelHeader, RnPanelBody} from './layout/panel/rn-panel.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RnAlert} from './messaging/alert/rn-alert.component';
import {BreadcrumbComponent} from './layout/breadcrumb/breadcrumb.component';
import {BreadcrumbService} from './layout/breadcrumb/breadcrumb.service';
import {SearchComponent} from './input/search.component';

@NgModule({
  imports:  [
    CommonModule,
    NgbModule,
    ImageCropperModule,
    RouterModule
  ],
  declarations: [
    RnPanel,
    RnPanelHeader,
    RnPanelBody,
    RnAlert,
    SearchComponent,
    ImageCropperModal,
    BreadcrumbComponent
  ],
  entryComponents: [
    ImageCropperModal
  ],
  providers : [
    BreadcrumbService
  ],
  exports : [
    RnPanel,
    RnPanelHeader,
    RnPanelBody,
    RnAlert,
    SearchComponent,
    ImageCropperModal,
    BreadcrumbComponent
  ]
})
export class RnComponentsModule{};
