import {Component, OnInit, Input, HostListener} from '@angular/core';

@Component({
    selector: 'scroll-anchor',
    template: `
        <a (click)="doScroll($event)"><ng-content></ng-content></a>
    `
})
export class ScrollAnchorComponent implements OnInit {
    @Input('fragment') anchor: string;
    constructor() { }

    ngOnInit() { }

    @HostListener('click', ['$event.target'])
    public doScroll(e : any) : void {
        e.preventDefault();
        e.stopPropagation();
        let el = document.querySelector('#' + this.anchor);
        let rect: ClientRect = el.getBoundingClientRect();
        this.smoothScrollTo(document.body, rect.top + window.scrollY, 500);
    }

    private smoothScrollTo(element, target, duration): void {
        target = Math.round(target);
        duration = Math.round(duration);

        if (duration < 0) {
            return;
        }
        if (duration === 0) {
            element.scrollTop = target;
            return;
        }

        let start_time = Date.now();
        let end_time = start_time + duration;

        let start_top = element.scrollTop;
        let distance = target - start_top;

        // based on http://en.wikipedia.org/wiki/Smoothstep
        let smooth_step = function (start, end, point) {
            if (point <= start) { return 0; }
            if (point >= end) { return 1; }
            let x = (point - start) / (end - start); // interpolation
            return x * x * (3 - 2 * x);
        };

        let previous_top = element.scrollTop;

        // This is like a think function from a game loop
        let scroll_frame = function () {
            if (element.scrollTop != previous_top) {
                return;
            }

            // set the scrollTop for this frame
            let now = Date.now();
            let point = smooth_step(start_time, end_time, now);
            let frameTop = Math.round(start_top + (distance * point));
            element.scrollTop = frameTop;

            // check if we're done!
            if (now >= end_time) {
                return;
            }

            if (element.scrollTop === previous_top
                && element.scrollTop !== frameTop) {
                return;
            }
            previous_top = element.scrollTop;

            // schedule next frame for execution
            setTimeout(scroll_frame, 0);
        }

        // boostrap the animation process
        setTimeout(scroll_frame, 0);
    }
}
