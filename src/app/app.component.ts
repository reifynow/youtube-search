import { Component } from '@angular/core';
import { AuthService } from './rn-secure/auth.service';
import { UserService } from './rn-main/account/user.service';
import { Router } from '@angular/router';
import {IndicatorService} from "./rn-secure/loading.component";

@Component({
  selector: 'my-app',
  template: `
    <router-outlet></router-outlet>
    <af-loading-indicator [active]="indicatorService.active"></af-loading-indicator>
  `,
})
export class AppComponent  {
  constructor(private authService: AuthService, private userService: UserService, private router : Router, public indicatorService : IndicatorService) {
    this.authService.onTimeout = (timeoutFn) => {
      this.router.navigate(['/logout']);
    };
    this.router.events.subscribe(changes => {
      this.authService.refreshSession();
    });
    if (authService.isLoggedIn())
//      this.authService.loadCache();

      userService.getUser().subscribe(resUser => {
        authService.setUser(resUser);
        authService.saveSession();
      }, res => {
        if (~res.message.indexOf('Authorization has been denied')) router.navigate(['/logout']);
      });
  }
}
