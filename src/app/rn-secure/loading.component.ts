import {Component, Input, Injectable} from '@angular/core';

@Component({
  selector : 'af-loading-indicator',
  template: `
    <div *ngIf="active" class='backdrop'></div>
    <div *ngIf="active" class="loading-indicator"><!--<img src="/app/images/site_icon.png" />--></div>
  `,
  styles: [`
    .backdrop {
      position: fixed;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
      z-index: 50000;
      background: rgba(255, 255, 255, 0.5);
    }
    .loading-indicator {
      z-index: 50001;
      position: fixed;
      width: 200px;
      height: 200px;
      top: calc(50% - 100px);
      left: calc(50% - 100px);
      background: url('/assets/images/loader.gif') center center no-repeat;
      text-align: center;
    }
    .loading-indicator img {
      margin-top: calc(50% - 30px);
      max-width: 60px;
      max-height: 60px;
      opacity: 0.75;
    }
  `]
})
export class LoadingIndicator {
  @Input() active : boolean = false;
  constructor() {}
}

@Injectable()
export class IndicatorService {
  active : boolean = false;
  constructor() {}
}
