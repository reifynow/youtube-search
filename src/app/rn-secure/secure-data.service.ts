import {Injectable} from '@angular/core';
import {Headers} from '@angular/http';
import {Http} from '@angular/http';
import {AuthService } from './auth.service';

@Injectable()
export class SecureDataService {

  constructor(private http: Http, private authService : AuthService) {
    this.http.get('/properties.json').map(res => {
      return res.json()
    }).subscribe(
      data => {
        for(var key in data) {
          localStorage.setItem(key, data[key]);
        }
      },
      error => {
        console.log(error);
      }
    );
  }


  getHeaders(noAuth : boolean = false) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    if (noAuth) return headers;
    if (this.authService.isLoggedIn()) {
      headers.append('Authorization', this.authService.authData.accessType + ' ' + this.authService.authData.accessToken);
    }
    return headers;
  }

  getYoutubeUrl(): string {
    return window.localStorage.getItem('YOUTUBE_API_URL');
  }

  getYoutubeKey(): string {
    return window.localStorage.getItem('YOUTUBE_API_KEY');
  }

  getApiUrl(): string {
    return window.localStorage.getItem('API_URL');
  }

  getAuthUrl() : string {
    return window.localStorage.getItem('AUTH_URL');
  }

  getUserApiUrl(): string {
    return window.localStorage.getItem('USER_API_URL');
  }

}
