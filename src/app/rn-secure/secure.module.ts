import {NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import {HttpClient} from './http-client';
import {SecureDataService } from './secure-data.service';
import {AuthGuard } from './auth-guard.service';
import {AuthService } from './auth.service';
import {IndicatorService, LoadingIndicator} from "./loading.component";

@NgModule({
  imports:  [
    CommonModule,
    HttpModule,
    RouterModule
  ],
  declarations: [
      LoadingIndicator
  ],
  providers : [
    SecureDataService,
    AuthGuard,
    HttpClient,
    AuthService,
    IndicatorService
  ],
  exports: [
      LoadingIndicator
  ]
})
export class SecureModule{};
