import {Injectable} from '@angular/core';
import {Observable}     from 'rxjs/Rx';
import {Http, RequestOptionsArgs, Response} from '@angular/http';
import {SecureDataService}    from './secure-data.service';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';
import {IndicatorService} from "./loading.component";
@Injectable()
export class HttpClient {

  private noAuthenticate: boolean = false;
  constructor(private http: Http, private _appDataService : SecureDataService, private indicatorService : IndicatorService) {
  }

  delete(url: string, options?: RequestOptionsArgs): Observable<any> {
    console.log('CustomHttp - DELETE');
    return this.http.delete(url, this.getOptions(options))
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }

  put(url: string, body: string, options?: RequestOptionsArgs): Observable<any> {
    console.log('CustomHttp - PUT');
    return this.http.put(url, body, this.getOptions(options, true))
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }

  post(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
    console.log('CustomHttp - POST');
    return this.http.post(url, body, this.getOptions(options, true))
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this));
  }


  get(url: string, options?: RequestOptionsArgs): Observable<any> {
    console.log('CustomHttp - GET');
    return this.http.get(url, this.getOptions(options))
                    .map(this.extractData.bind(this))
                    .catch(this.handleError.bind(this))
  }

  noAuth() : HttpClient {
    this.noAuthenticate = true;
    return this;
  }

  private getOptions(options?: RequestOptionsArgs, excludeBody : boolean = false): RequestOptionsArgs {
    this.indicatorService.active = true;
    let headersGlobal = this._appDataService.getHeaders(this.noAuthenticate);
    if (this.noAuthenticate) this.noAuthenticate = false;
    let retOptions = {};
    if (excludeBody) retOptions = {headers: headersGlobal};
    else retOptions = { headers: headersGlobal, body : '' };
    if (options) Object.keys(options).forEach(key => {
      retOptions[key] = options[key];
    });
    return retOptions;
  }

  private extractData(res: Response) : any {
    this.indicatorService.active = false;
    if (res.status < 200 || res.status >= 300) {
      return Observable.throw(res);
    }
    return res;
  }

  private handleError (error : Response | any) : Observable<Response | any> {
    this.indicatorService.active = false;
    let err = error;
    let message = {Message : false};
    let res : any = {type: 'danger', message: 'Server error.', dismissible: false, timeToLive: null};
//    if (err.status == 401) this.router.navigate(['/logout']);
    if (err.status == 404) return Observable.throw(err);
    if (err._body) {
      return Observable.throw(err);
    }
    if (message.Message) res = {type: 'danger', message: message.Message, dismissible: true, timeToLive: null};
    return Observable.throw(res);
/*    if (res.status == 401) {
    } else {
      return Observable.throw(res.json().error || 'Server error');
    } */
  }
}
