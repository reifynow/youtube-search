import {NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';

import {RnCoreModule} from '../rn-core/rn-core.module';
import {RnMainRoutingModule} from './rn-main.routing.module';
import {RnMainComponent} from './rn-main.component';
import {AccountModule} from './account/account.module';
import {RnMainService} from './rn-main.service';
import {HomeModule} from './home/home.module';

import {DashboardModule} from './dashboard/dashboard.module';
//Providers


@NgModule({
  imports:  [
    CommonModule,
    FormsModule,
    RnCoreModule,
    RnMainRoutingModule,
    AccountModule,
    DashboardModule,
    HomeModule
  ],
  declarations: [
    RnMainComponent
  ],
  providers : [
    RnMainService
  ],
  exports : [
    RnMainComponent
  ]
})
export class RnMainModule{};
