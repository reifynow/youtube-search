import {Component, OnInit, ViewChild} from '@angular/core';
import {FormGroup, FormBuilder} from '@angular/forms';
import {LoginService} from './login.service';
import {AuthService} from '../../../rn-secure/auth.service';
import {RnAlertData} from '../../../rn-core/components/messaging/alert/rn-alert.component';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {RnValidationService} from '../../../rn-core/rn-validation.service';
import {UserService} from '../user.service';
import {UserProfile } from '../../dashboard/user';

@Component({
  selector: 'rn-login',
  templateUrl: './login.component.html',
  providers: [LoginService],
  styleUrls: ['./styles.css']
})
export class LoginComponent implements OnInit {
  loginTitle : string = "";
  user : any= {username: '', password: ''};
  password: string = '';
  isLoggedIn : boolean = false;
  alerts : Array<RnAlertData> = [];
  loginForm: FormGroup;
  validationService: RnValidationService;

  constructor(private loginService : LoginService, private authService : AuthService, private router : Router, private route : ActivatedRoute, private fb : FormBuilder, private userService : UserService) {

    if (this.authService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.alerts.push({type: 'success', message: 'You\'re already logged in!', dismissible: false, timeToLive: null});
      router.navigate(['/dashboard']);
    }
  }

  ngOnInit() {
    this.validationService = new RnValidationService(this.loginForm, this.fb);
    this.loadFormElements();
  }

  doLogin() {
    this.validationService.validateCleanForm();
    this.user.username = this.loginForm.value.username;
    this.user.password = this.loginForm.value.password;
    this.alerts = [];
    if (!this.loginForm.valid) {
      let message : Array<string> = [];
      Object.keys(this.validationService.formErrors).forEach(key => {
        let msg = this.validationService.formErrors[key].message;
        if (msg && msg.length) message.push(msg);
      });
      this.alerts.push({type: 'danger', message: message.join(' '), dismissible: true, timeToLive: null});
    }
    else {
      //TODO: Remove jontest user login hack.
        this.loginService.login(this.user.username, this.user.password).subscribe(result => {
          if (!result) {
            this.alerts.push({type: 'danger', message: 'Login Failed!', dismissible: true, timeToLive: null});
          } else {
            //TODO: Usually result is the result of an oAuth request which contains timeout information. This result is just the user object so we're omitting the oAuth result which forces default values to be used..
            this.authService.setLoggedIn({accessToken: result.token, accessType: 'Bearer', expires: 36000}, result);
            this.authService.setUser(result);
            this.authService.saveSession();
            this.router.navigate(['/dashboard']);
/*            this.userService.getUser().subscribe(user => {
              this.authService.setUser(user);
              this.authService.saveSession();
              this.router.navigate(['/dashboard']);
            }); */
          }
        }, err => {
          this.alerts.push(<RnAlertData>err);
        });
    }
  }

  loadFormElements() {
    this.loginForm = this.validationService.loadFormObjects([
      {
        name: 'username',
        prettyName: 'User name ',
        value: this.user.username,
        validations: ['required']
      },{
        name: 'password',
        prettyName: 'Password ',
        value: this.user.password,
        validations: ['required']
      }
    ]);
  }

}
