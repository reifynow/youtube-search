import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {RnCoreModule} from '../../rn-core/rn-core.module';
import {UserService} from './user.service';
import {RegisterComponent} from './register/register.component';
import {RegisterService} from './register/register.service';
import {LoginComponent} from './login/login.component';
import {LogoutComponent} from './login/logout.component';
import {AccountRoutingModule} from './account.routing.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    RnCoreModule,
    AccountRoutingModule
  ],
  declarations: [
    RegisterComponent,
    LoginComponent,
    LogoutComponent
  ],
  entryComponents: [
    RegisterComponent
  ],
  providers: [
    UserService,
    RegisterService
  ],
  exports: [
    LoginComponent
  ]
})
export class AccountModule {};
