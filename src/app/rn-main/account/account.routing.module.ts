import { NgModule} from '@angular/core';
import { RouterModule} from '@angular/router';
import { LogoutComponent } from './login/logout.component';
import {RegisterComponent} from './register/register.component';
@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'logout',
        component: LogoutComponent
      },
      {
        path: 'register',
        component : RegisterComponent
      }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class AccountRoutingModule {}
