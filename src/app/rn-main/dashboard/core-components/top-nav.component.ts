import {Component, Input} from '@angular/core';

@Component({
  selector: 'top-nav',
  template: `
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">RN Sample</a>
    </div>

    
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li routerLinkActive="active"><a routerLink="home">Home</a></li>
      <!--  <li routerLinkActive="active"><a routerLink="users">Users</a></li>-->
      <li routerLinkActive="active"><a routerLink="search">Search Videos</a></li>
          <li routerLinkActive="active"><a routerLink="favorites">Manage Favorites</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{userProfile.firstName}} {{userProfile.lastName}} <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a routerLink="/logout">Logout</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>
  `,
  styles: [`
    .nav-top-content {width: 257px;}
  `]
})
export class TopNavComponent {
  @Input('user') userProfile = {firstName: '', lastName: ''};

  userprofile() {}

  stopLink(e : any) : void {
    e.stopPropagation();
    e.preventDefault();
    return;
  }
}
