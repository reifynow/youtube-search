import {Component, Input, Output} from '@angular/core';

export class PageTab {
  title: string = '';
  navigation : any = null;
  active : boolean = false;
  constructor(title: string, navigation: any) {
    this.title = title;
    this.navigation = navigation;
  }
}

@Component({
  selector: 'page-tabs',
  template: `
			<div class="st-page-navigation">
				<ul role="tablist">
				  <li *ngFor="let tab of tabs" role="presentation" routerLinkActive="active"><a href="" [routerLink]="tab.navigation">{{tab.title}}</a></li>
				</ul>
			</div>
  `
})
export class PageTabsComponent {
  @Input() tabs : Array<PageTab> = [];
  constructor() {

  }
}
