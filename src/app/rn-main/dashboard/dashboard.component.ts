import {Component} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {AuthService} from '../../rn-secure/auth.service';
//import {UserProfile} from './user-profile/user';
@Component({
  selector: 'lci-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent {
  user : any = {};
  constructor(private router : Router, private route : ActivatedRoute, private authService : AuthService) {
    authService.getUser.subscribe(user => {
      this.user = user;
    });
  }

  logout() {
    this.router.navigate(['/logout']);
  }

  userprofile() {
    this.router.navigate(['./userprofile'], {relativeTo: this.route});
  }
}
