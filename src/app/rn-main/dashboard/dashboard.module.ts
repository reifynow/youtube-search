import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {RnCommonModule} from '../../rn-common/rn-common.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RouterModule} from '@angular/router';
import {CoreComponentsModule} from './core-components/core-components.module';
import {DashboardComponent} from './dashboard.component';
import {ButtonModule, CheckboxModule, InputTextModule, PanelModule} from 'primeng/primeng';
import {RnCoreModule} from "../../rn-core/rn-core.module";
import {HomeComponent} from "./manage/home.component";
import {UsersComponent} from "./manage/users.component";
import {DashboardService} from "./dashboard.service";
import {SearchVideosComponent} from "./manage/search-videos.component";
import {YoutubeService} from "./youtube.service";
import {ViewVideoComponent} from "./manage/view-video.component";
import {FavoritesService} from "./favorites.service";
import {FavoritesComponent} from "./manage/favorites.component";
import {FavoriteButtonComponent} from "./manage/favorite-button.component";

@NgModule({
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        RnCommonModule,
        RnCoreModule,
        CoreComponentsModule,
        CheckboxModule,
        PanelModule,
        ButtonModule,
        InputTextModule
    ],
    declarations: [
        DashboardComponent,
        HomeComponent,
        UsersComponent,
        SearchVideosComponent,
        ViewVideoComponent,
        FavoritesComponent,
        FavoriteButtonComponent
    ],
    entryComponents: [],
    providers: [
        DashboardService,
        YoutubeService,
        FavoritesService
    ]
})
export class DashboardModule {
}
