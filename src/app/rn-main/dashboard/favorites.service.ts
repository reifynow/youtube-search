import {Injectable} from '@angular/core';
import {AuthService} from "../../rn-secure/auth.service";

@Injectable()
export class FavoritesService {
    constructor(private authService : AuthService) {

    }

    loadFavorites() : Array<any> {
        return this.authService.sessionData['favorites'] || [];
    }

    addFavorite(video : any) {
        let favorites = this.loadFavorites();
        favorites.push(video);
        this.authService.setSessionItem('favorites', favorites);
        this.authService.saveSession();
    }

    removeFavorite(videoId : any) {
        let favorites = this.loadFavorites();
        let idx = favorites.findIndex(video => {
            return video.id == videoId;
        });
        if (~idx) favorites.splice(idx, 1);
        this.authService.setSessionItem('favorites', favorites);
        this.authService.saveSession();
    }

    isFavorite(videoId: string) {
        let favorites = this.loadFavorites();
        let idx = favorites.findIndex(video => {
            return video.id == videoId;
        });
        return ~idx;
    }
}