import {Injectable} from '@angular/core';
import {URLSearchParams} from '@angular/http';
import {HttpClient} from '../../rn-secure/http-client';
import {Observable} from "rxjs/Observable";
import {SecureDataService} from "../../rn-secure/secure-data.service";

@Injectable()
export class YoutubeService {
    youtubeUrl : string;
    youtubeKey : string;
    constructor(private http: HttpClient, private secureDataService : SecureDataService) {
        this.youtubeUrl = this.secureDataService.getYoutubeUrl();
        this.youtubeKey = this.secureDataService.getYoutubeKey();
    }

    searchVideos(q : string, maxResults: number, order: string, pageToken: string): Observable<any> {
        let p = new URLSearchParams();
        p.set('key', this.youtubeKey);
        p.set('part', 'snippet');
        p.set('q', q);
        p.set('type', 'video');
        p.set('pageToken', pageToken.toString());
        p.set('maxResults', maxResults.toString());
        p.set('order', order);
        return this.http.noAuth().get(this.youtubeUrl + '/search', {search: p}).map(res => res.json());
    }

    getVideo(videoId: string): Observable<any> {
        let p = new URLSearchParams();
        p.set('key', this.youtubeKey);
        p.set('part', 'snippet,statistics');
        p.set('id', videoId);
        return this.http.noAuth().get(this.youtubeUrl+'/videos', {search: p}).map(res => res.json());
    }

    getVideoComments(videoId: string, pageToken: string): Observable<any> {
        let p = new URLSearchParams();
        p.set('key', this.youtubeKey);
        p.set('part', 'snippet,replies');
        p.set('videoId', videoId);
        p.set('pageToken', pageToken);
        return this.http.noAuth().get(this.youtubeUrl+'/commentThreads', {search: p}).map(res => res.json());
    }
}