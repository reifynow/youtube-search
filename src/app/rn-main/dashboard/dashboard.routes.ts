import {Routes} from '@angular/router';
import {DashboardComponent} from './dashboard.component';
import {HomeComponent} from "./manage/home.component";
import {UsersComponent} from "./manage/users.component";
import {SearchVideosComponent} from "./manage/search-videos.component";
import {ViewVideoComponent} from "./manage/view-video.component";
import {FavoritesComponent} from "./manage/favorites.component";

export const DashboardRoutes: Routes = [{
  path: '',
  component: DashboardComponent,
  children: [{
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },{
    path: 'home',
    component: HomeComponent
  },{
    path: 'users',
    component: UsersComponent
  },{
    path: 'search',
    component: SearchVideosComponent
  },{
    path: 'video',
    component: ViewVideoComponent
  },{
    path: 'favorites',
    component: FavoritesComponent
  }]
}];
