import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {FavoritesService} from "../favorites.service";

@Component({
    selector: 'favorite-button',
    template: `
        <button pButton type="button" [class.ui-button-danger]="isFavorite" [icon]="getFavoriteIcon()" iconPos="right" (click)="handleFavorite()" label="Favorite"></button>
    `
})
export class FavoriteButtonComponent implements OnInit, OnChanges {
    @Input() videoDetails = null;
    @Output() favoriteRemoved : EventEmitter<any> = new EventEmitter();
    @Output() favoriteAdded : EventEmitter<any> = new EventEmitter();
    isFavorite: boolean = false;
    constructor(private favoriteService : FavoritesService) {}

    ngOnInit() {
        if (this.favoriteService.isFavorite(this.videoDetails.id)) this.isFavorite = true;
        else this.isFavorite = false;
    }

    ngOnChanges() {
        if (this.favoriteService.isFavorite(this.videoDetails.id)) this.isFavorite = true;
        else this.isFavorite = false;
    }

    getFavoriteIcon() {
        if (this.isFavorite) {
            return 'fa-minus';
        }
        else {
            return 'fa-plus';
        }
    }

    handleFavorite() {
        if (this.isFavorite) {
            this.favoriteService.removeFavorite(this.videoDetails.id);
            this.isFavorite = false;
            this.favoriteRemoved.emit(this.videoDetails);
        }
        else {
            this.favoriteService.addFavorite(this.videoDetails);
            this.isFavorite = true;
            this.favoriteAdded.emit(this.videoDetails);
        }
    }
}