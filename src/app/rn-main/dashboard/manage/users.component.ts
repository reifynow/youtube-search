import {Component, OnInit, OnDestroy} from '@angular/core';
import {DashboardService} from '../dashboard.service';
import {RnAlertData} from "../../../rn-core/components/messaging/alert/rn-alert.component";
import {Observable} from 'rxjs/Rx';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ModalResult, ConfirmModalComponent} from '../../../rn-core/components/modal/confirm-modal.component';
@Component({
    selector: 'db-users',
    template: `
<div class="panel panel-primary">
<div class="panel-heading">Users</div>
<div class="panel-body">
        <div class='row'>
            <div class="col-md-12" style="padding-left: 25px; padding-right: 25px;">
                <rn-alert [alerts]="alerts"></rn-alert>
            </div>
            <div class="col-md-6 col-md-offset-3">
                <div *ngFor="let user of users">
                    <p-checkbox  name="selectedUsers" [value]="user._id" [label]="user.firstName + ' ' + user.lastName" [(ngModel)]="selectedUsers"></p-checkbox><br />
                </div>
                <button class="btn" (click)="deleteUsers()">Delete Selected</button>
            </div>
        </div>
        </div>
        </div>
    `
})
export class UsersComponent implements OnInit, OnDestroy {
    users : Array<any> = [];
    selectedUsers : Array<string> = [];
    alerts : Array<RnAlertData> = [];
    constructor(private dashboardService : DashboardService, private modal : NgbModal) {

    }

    ngOnInit() {
        this.dashboardService.getUsers().subscribe(users => {
            this.users = users;
        });
    }

    ngOnDestroy() {
    }

    deleteUsers() {
        let modal = this.modal.open(ConfirmModalComponent);
        let instance = <ConfirmModalComponent>modal.componentInstance;
        instance.title = 'Delete Users';
        instance.content = `<p>Are you sure?</p>`;
//TODO: reinstate when convert over to bootstrap 4 (ng-bootstrap is using 4, and recent releases have broken styles for this among other things).
//        modal.result.then((res : ModalResult) => {
//            if (res == ModalResult.Ok) {
                let delAll : Array<any> = [];
                this.selectedUsers.forEach((id : string) => {
                    delAll.push(this.dashboardService.deleteUser(id));
                });

                Observable.forkJoin.apply(null, delAll).subscribe(res => {
                    this.dashboardService.getUsers().subscribe(users => {
                        this.users = users;
                        this.alerts.push(new RnAlertData('success', 'Users successfully deleted!'));
                    });

                });
//            }
//        }, res => {});
    }
}