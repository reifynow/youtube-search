import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {DomSanitizer} from "@angular/platform-browser";
import {YoutubeService} from "../youtube.service";
import {Observable} from "rxjs/Observable";
import {FavoritesService} from "../favorites.service";

@Component({
    selector: 'view-video',
    template: `
        <div class="row">
        <div class="col-md-12">
            <div style="width: 900px; padding: 23px; margin: 0 auto;">
                <h4>{{videoDetails.title}}</h4>
        <div style="width: 854px; height: 510px; margin: 0 auto;">
            <iframe width="854" height="510" [src]="getVideoUrl()" frameborder="0" allowfullscreen></iframe>
        </div>
                <p style="float:left;"><strong>Channel:</strong> <a target="_blank" href="https://www.youtube.com/channel/{{videoDetails.channelId}}">{{videoDetails.channelTitle}}</a></p>
                <p style="float:right; margin-top: 5px;">
                    <button pButton type="button" class="ui-button-secondary" icon="fa-thumbs-up" label="{{videoStatistics.likeCount}}" iconPos="left"></button>
                    <button pButton type="button" class="ui-button-secondary" icon="fa-thumbs-down" label="{{videoStatistics.dislikeCount}}" iconPost="left"></button>
                </p>
                <p style="clear: both; float: right; margin-top: 5px;"><favorite-button [videoDetails]="favoriteDetails"></favorite-button></p>
                <span style="display:block; clear:both;"></span>
                <p-panel>
                    <p-header>Comments</p-header>
                    <ul>
                        <li *ngFor="let comment of comments">
                            <p style="font-size: 10px;">{{comment.publishedAt | date:'shortDate'}} by {{comment.authorDisplayName}}</p>
                            <p [innerHtml]="comment.textDisplay"></p>
                        </li>
                    </ul>
                    <p-footer>
                        <button pButton type="button" label="Previous" (click)="prevPage()" style="float: left;"></button>
                        <button pButton type="button" label="Next" (click)="nextPage()" style="float: right;"></button>
                        <span style="clear:both; margin-top: 5px; margin-bottom: 5px; display:block;"></span>
                    </p-footer>
                </p-panel>
            </div>
</div>
        </div>
    `,
    styles: [`
        ul {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        ul li {
            padding: 25px;
            border-bottom: 1px solid #ccc;
        }

        ul li:last-child {
            border-bottom: none;
        }
    `]
})
export class ViewVideoComponent implements OnInit {
    videoId: string;
    videoUrl: string;
    videoDetails: any = {};
    comments : Array<any> = [];
    prevPageToken: string = '';
    nextPageToken: string = '';
    videoStatistics: any = {};
    favoriteDetails : any = {};
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private sanitizer: DomSanitizer,
        private youtubeService : YoutubeService,
        private favoriteService : FavoritesService
    ) {

    }

    pageChange(pageToken) {
        this.loadComments(pageToken).subscribe();
    }

    loadComments(pageToken?) : Observable<any> {
        return this.youtubeService.getVideoComments(this.videoId, pageToken).do(res => {
            this.nextPageToken = res.nextPageToken;
            this.prevPageToken = res.prevPageToken;
            this.comments = res.items.map(item => {
                return item.snippet.topLevelComment.snippet;
            });
            return Observable.of(res);
        });
    }


    getVideoUrl() {
        return this.sanitizer.bypassSecurityTrustResourceUrl(this.videoUrl);
    }


    ngOnInit() {
        this.route.params.forEach((params : Params) => {
            if (params['videoId']) {
                this.videoId = params['videoId'];
                this.videoUrl = "http://www.youtube.com/embed/"+params['videoId'];
                this.loadVideoDetails()
            }
        });
    }

    loadVideoDetails() {
        Observable.forkJoin(this.youtubeService.getVideo(this.videoId), this.loadComments()).subscribe(res => {
            this.videoDetails = res[0].items[0].snippet;
            this.favoriteDetails = res[0].items[0];
            this.videoStatistics = res[0].items[0].statistics;
        });
    }

    prevPage() {
        this.pageChange(this.prevPageToken);
    }

    nextPage() {
        this.pageChange(this.nextPageToken);
    }
}