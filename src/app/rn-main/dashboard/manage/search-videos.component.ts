import {Component, OnInit} from '@angular/core';
import {FormControl} from "@angular/forms";
import {YoutubeService} from "../youtube.service";
import {Observable} from "rxjs/Observable";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'search-videos',
    template: `

        <div class="row" style="width: 400px; margin: 0 auto;">
            <p-panel>
                <p-header>
                    <form class='horizontal-form'>
                        <div class="ui-inputgroup" style="width: 265px; margin: 0 auto;">
                            <button pButton type="button"  label="Search"></button>
                            <input type="text" pInputText placeholder="Keyword..." [formControl]="query">
                        </div>
                    </form>
                </p-header>
                <p class="small">
                    <button pButton (click)="setSortBy('date')" class="ui-secondary" type="button" [icon]="getSortIcon('date')" label="Date" iconPos="right"></button>
                    <button pButton (click)="setSortBy('rating')" class="ui-secondary" type="button" [icon]="getSortIcon('rating')" label="Rating" iconPos="right"></button>
                    <button pButton (click)="setSortBy('relevance')" class="ui-secondary" type="button" [icon]="getSortIcon('relevance')" label="Relevance" iconPos="right"></button>
                </p>
                <ul>
                    <li *ngFor="let video of videos">
                        <a click="viewVideo($event, video)">{{video.snippet.title}}</a>
                        <a style="display:block; width: 320px; height: 180px; margin: 10px auto;" href
                           (click)="viewVideo($event, video)"><img style="width: 320px; height: 180px;"
                                                                   [src]="video.snippet.thumbnails.medium.url"/></a>
                    </li>
                </ul>

                <p-footer>
                    <button pButton type="button" label="Previous" (click)="prevPage()" style="float: left;"></button>
                    <button pButton type="button" label="Next" (click)="nextPage()" style="float: right;"></button>
                    <span style="clear:both; margin-top: 5px; margin-bottom: 5px; display:block;"></span>
                </p-footer>
            </p-panel>
        </div>
    `,
    styles: [`
        ul {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        ul li {
            padding: 25px;
            border-bottom: 1px solid #ccc;
        }
        
        ul li:last-child {
            border-bottom: none;
        }
    `]
})
export class SearchVideosComponent implements OnInit {
    query: FormControl = new FormControl();
    videos: Array<any> = [];
    prevPageToken: string = '';
    nextPageToken: string = '';
    collectionSize: number = 0;
    maxResults: number = 5;
    q: string = '';
    sortingBy: string = 'relevance';

    constructor(private youtubeService: YoutubeService, private router: Router, private route: ActivatedRoute) {

    }

    setSortBy(sortBy: string) {
        this.sortingBy = sortBy;
        this.pageChange();
    }

    ngOnInit() {
        this.query.valueChanges.debounceTime(1000).distinctUntilChanged().subscribe(val => {
            this.q = val;
            this.pageChange();
        });
    }

    viewVideo(e: any, video) {
        if (e) {
            e.stopPropagation();
            e.preventDefault();
        }
        this.router.navigate(['../video', {videoId: video.id.videoId}], {relativeTo: this.route});
    }

    prevPage() {
        this.pageChange(this.prevPageToken);
    }

    nextPage() {
        this.pageChange(this.nextPageToken);
    }

    pageChange(pageToken: string = '') {
        this.youtubeService.searchVideos(this.q, this.maxResults, this.sortingBy, pageToken).subscribe(res => {
            this.collectionSize = res.pageInfo.totalResults;
            this.videos = res.items;
            this.nextPageToken = res.nextPageToken;
            this.prevPageToken = res.prevPageToken;
        });
    }

    getSortIcon(sortBy : string) : string {
        if (sortBy == this.sortingBy) return 'fa-sort-down';
        return 'fa-sort';
    }
}
