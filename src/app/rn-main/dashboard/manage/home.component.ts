import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs/Rx';
import {AuthService} from '../../../rn-secure/auth.service';

@Component({
    selector: 'db-home',
    template: `
    <div class="jumbotron">
  <h1>Hello, {{user.firstName}} {{user.lastName}}</h1>
  <p>This is a tool to search youtube and manage favorite videos.</p>
  <p><a class="btn btn-primary btn-lg" routerLink="../search" role="button">Go To Search</a> or <a class="btn btn-primary btn-lg" routerLink="../favorites" role="button">View Your Favorites</a></p>
</div>
    `
})
export class HomeComponent implements OnInit, OnDestroy {
    user : any = {firstName : '', lastName: ''};
    subscriptions : Array<Subscription> = [];
    constructor(private authService : AuthService) {

    }

    ngOnInit() {
        this.subscriptions.push(this.authService.getUser.subscribe(user => {
            this.user = user;
        }));
    }

    ngOnDestroy() {
        this.subscriptions.forEach((subscription: Subscription) => {
            subscription.unsubscribe();
        });
    }
}