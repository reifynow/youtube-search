import {Component, OnInit} from '@angular/core';
import {FavoritesService} from "../favorites.service";

@Component({
    selector: 'favorites',
    template: `
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <p-panel>
                    <p-header>Manage Favorites</p-header>
                    <ul>
                        <li *ngFor="let video of favorites">
                            <p style="text-align: right;"><favorite-button [videoDetails]="video" (favoriteRemoved)="loadFavorites($event)"></favorite-button></p>
                            <a style="display:block; width: 320px; height: 180px; margin: 10px auto;" href
                               (click)="viewVideo($event, video)"><img style="width: 320px; height: 180px;"
                                                                       [src]="video.snippet.thumbnails.medium.url"/></a>
                            <p style="text-align: center;">
                                <a click="viewVideo($event, video)">{{video.snippet.title}}</a>
                            </p>
                        </li>
                    </ul>
                </p-panel>
            </div>
        </div>
    `,
    styles: [`
        ul {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        ul li {
            padding: 25px;
            border-bottom: 1px solid #ccc;
        }

        ul li:last-child {
            border-bottom: none;
        }
    `]
})
export class FavoritesComponent implements OnInit {
    favorites: Array<any> = [];

    constructor(private favoriteService: FavoritesService) {
    }

    ngOnInit() {
        this.loadFavorites();
    }

    loadFavorites() {
        this.favorites = this.favoriteService.loadFavorites();
    }

}