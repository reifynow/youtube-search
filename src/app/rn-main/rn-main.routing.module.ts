import { NgModule} from '@angular/core';
import { RouterModule} from '@angular/router';
import { RnMainComponent } from './rn-main.component';
import { HomeRoutes } from './home/home.routes';
import {AuthGuard} from '../rn-secure/auth-guard.service';
import {DashboardRoutes} from './dashboard/dashboard.routes';
@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: RnMainComponent,
        children: HomeRoutes
      },
      {
        path: 'dashboard',
        canActivate: [AuthGuard],
        children: DashboardRoutes
      }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class RnMainRoutingModule {}
