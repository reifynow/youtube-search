import { Component, OnInit, ViewChild } from '@angular/core';
import {AuthService} from '../../rn-secure/auth.service';
import {UserService} from '../account/user.service';
import {RnAlertData} from '../../rn-core/components/messaging/alert/rn-alert.component';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {FormGroup, FormBuilder} from '@angular/forms';
import {RnValidationService} from '../../rn-core/rn-validation.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {RegisterComponent} from '../account/register/register.component';
import {RegisterService} from '../account/register/register.service';
@Component({
    templateUrl: './home.component.html',
    providers: [RegisterService],
    styleUrls: ['./styles.css']
})
export class HomeComponent implements OnInit {
    loginModel: LoginModel = new LoginModel();
    alerts: Array<RnAlertData> = [];
    loginForm: FormGroup;
    displayRegister : boolean = false;
    validationService: RnValidationService;
    @ViewChild('content') content;
    constructor(private authService: AuthService, private userService: UserService, private router: Router, private formBuilder: FormBuilder, private modalService : NgbModal, private route : ActivatedRoute, private registerService: RegisterService) {
      route.queryParams.forEach((params: Params) => {
/*        if (params['user_id'] && params['code']) {
          console.log(params['user_id'] + ' ' + params['code']);
          this.registerService.registerStepOne(params['user_id'], params['code']).subscribe(res => {
            let modal = this.modalService.open(RegisterComponent, {windowClass: 'lci-modal'});
            let componentInstance = <RegisterComponent>modal.componentInstance;
            modal.result.then(res => {
              if (res == 'dashboard') router.navigate(['/dashboard']);
            }, () => {});
            componentInstance.tab = 4;
            componentInstance.userObject = res;
          }, res => {
            let modal = this.modalService.open(RegisterComponent, {windowClass: 'lci-modal'});
            let componentInstance = <RegisterComponent>modal.componentInstance;
            modal.result.then(res => {
              if (res == 'dashboard') router.navigate(['/dashboard']);
            }, () => {});
            componentInstance.tab = 14;
            componentInstance.resendTab = 3;
          });
        }*/
      });
    }

    ngOnInit() {
    }

    loadRegister(tab ?: number) : void {

      let modal = this.modalService.open(RegisterComponent, {windowClass: 'rn-modal'});
      let componentInstance = <RegisterComponent>modal.componentInstance;
      modal.result.then(res => {
        if (res == 'dashboard') this.router.navigate(['/dashboard']);
      }, () => {});
//      if (tab) componentInstance.tab = tab;
    }
}

class LoginModel {
    public username: string;
    public password: string;
}
