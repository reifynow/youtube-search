import {Component, Input} from '@angular/core';

//TODO: FINISH THIS COMPONENT - jh
@Component({
  selector: 'breadcrumb',
  template: `
      <ul class="navigation-list-history">
        <li><a href="">HOME</a></li>
        <li><a href="">USER PROFILE</a></li>
        <li><a href="">{{location}}</a></li>
      </ul>
  `
})
export class BreadcrumbComponent {
  @Input() location: string = '';
  constructor() {

  }
}
