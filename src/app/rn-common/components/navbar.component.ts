import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'navbar',
  template: `<div class='bar' [ngClass]="cssClass"><ng-content></ng-content></div>`,
  styles: [`
    div.bar {
      position: absolute;
    }
    
    div.bar.top {
      left: 0;
      right: 0;
      top: 0;
      height: 60px;
    }
  `]
})
export class NavbarComponent implements OnInit {
  @Input() location : string = 'top';
  cssClass : any = {};
  constructor() {
  }

  ngOnInit() {
    this.cssClass[this.location] = true;
  }
}
