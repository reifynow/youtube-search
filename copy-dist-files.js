var fs = require('fs');
var resources = [
  'src/environments/properties.json',
  'src/environments/index.html'
];
resources.map(function(f) {
  var path = f.split('/');
  var t = 'dist/' + path[path.length-1];
  fs.createReadStream(f).pipe(fs.createWriteStream(t));
});
