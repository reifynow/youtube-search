var gulp = require('gulp');
var exec = require('child_process').exec;
var rename = require('gulp-rename');
var runSequence = require('run-sequence');
var util = require('gulp-util');
var bundle = require('gulp-bundle-assets');
var del = require('del');
var q = require('q');

var distDir = './dist';

gulp.task('default', function() {});

gulp.task('bundle', function() {
  gulp.src('./src/assets/images/**/*.*')
    .pipe(gulp.dest(distDir+'/assets/images'));
  gulp.src('./node_modules/font-awesome/fonts/**/*')
      .pipe(gulp.dest(distDir+'/public/fonts'));
  return gulp.src('./bundle.config.js')
    .pipe(bundle())
    .pipe(gulp.dest(distDir+'/public/assets'));
});

gulp.task('clean-bundles', function() {
  return del([
    distDir+'/public'
  ]);
});

gulp.task('clean-aot', function() {
  return del(['./aot']);
});

gulp.task('clean-build', function() {
  return del([distDir]);
});

gulp.task('clean', function(cb) {
  runSequence('clean-aot', 'clean-build', cb);
});
